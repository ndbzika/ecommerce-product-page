import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: 'Kumbh Sans', sans-serif;
  }

  html, body {
    height: 100%;
    width: 100%;
    overflow-x: hidden;
  }

  a, button {
    cursor: pointer;
  }

  a {
    text-decoration: none;
    color: #000;
  }

  button {
    border: none;
    background: none;
  }

  ul {
    list-style: none;
    padding: 0;
  }

  img {
    max-width: 100%;
    height: auto;
  }

  input, textarea {
    border: none;
    outline: none;
    background: none;
  }

`;

export default GlobalStyles;
