import { describe, expect, test } from "vitest";
import { render, screen } from "@testing-library/react";
import App from "../App";

describe("App", () => {
  test("should have render the header", () => {
    render(<App />);

    expect(screen.getByTestId("header")).toBeDefined();
  });
});
