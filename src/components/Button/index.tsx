import { ReactNode } from "react";
import { StyledButton } from "./styled";

type ButtonProps = {
  children: ReactNode | string;
  onClickFn?: () => void;
  w?: string;
  h?: string;
};

export const Button = ({ children, onClickFn, w, h }: ButtonProps) => {
  return (
    <StyledButton onClick={onClickFn} width={w} height={h}>
      {children}
    </StyledButton>
  );
};
