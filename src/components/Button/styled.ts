import styled from "styled-components";

interface StyledButtonProps {
  width?: string;
  height?: string;
}

export const StyledButton = styled.button<StyledButtonProps>`
  ${({ width = "100%", height = "100%" }) => {
    return `
    width: ${width};
    height: ${height};
  `;
  }}
  background-color: hsl(26, 100%, 55%);
  color: hsl(0, 0%, 100%);
  cursor: pointer;
  font-size: 1rem;
  font-weight: 700;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 0.5rem;
  border-radius: 0.6rem;
  transition: background-color 0.2s ease-in-out;

  img {
    width: 1.2rem;
    height: 1.2rem;
  }

  &:hover {
    background-color: hsl(26, 100%, 65%);
  }
`;
