import { HeaderIconButton } from "./HeaderIconButton";
import { HeaderIconsContainer } from "./HeaderIconsContainer";
import { HeaderListItems } from "./HeaderListItems";
import { HeaderItem } from "./HeaderItem";
import { HeaderLogo } from "./HeaderLogo";
import { HeaderRoot } from "./HeaderRoot";

export const Header = {
  Root: HeaderRoot,
  Logo: HeaderLogo,
  ListItems: HeaderListItems,
  Item: HeaderItem,
  IconsContainer: HeaderIconsContainer,
  IconButton: HeaderIconButton,
};
