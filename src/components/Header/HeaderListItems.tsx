import { ListItems } from "./styled";

export const HeaderListItems = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  return (
    <li data-testid="header-list-item">
      <ListItems>{children}</ListItems>
    </li>
  );
};
