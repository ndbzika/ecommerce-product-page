import { Item } from "./styled";

type HeaderItemTypes = {
  children: React.ReactNode;
  link: string;
};

export const HeaderItem = ({ children, link }: HeaderItemTypes) => {
  return (
    <Item>
      <a href={link} data-testid="header-item">
        {children}
      </a>
    </Item>
  );
};
