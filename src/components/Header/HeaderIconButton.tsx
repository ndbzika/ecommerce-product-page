import { Icon } from "./styled";

export const HeaderIconButton = ({
  icon,
  size,
}: {
  icon: "cart" | "profile";
  size?: string;
}) => {
  return (
    <li>
      <button>
        <Icon
          src={
            (icon === "cart" && `/images/icon-cart.svg`) ||
            `/images/image-avatar.png`
          }
          alt={`${icon} icon`}
          data-testid="icon"
          size={size}
        />
      </button>
    </li>
  );
};
