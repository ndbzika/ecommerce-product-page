import { ReactNode } from "react";
import { Header, List } from "./styled";

export const HeaderRoot = ({ children }: { children: ReactNode }) => {
  return (
    <Header data-testid="header">
      <List>{children}</List>
    </Header>
  );
};
