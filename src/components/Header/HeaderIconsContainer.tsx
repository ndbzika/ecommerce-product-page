import { IconContainer } from "./styled";

export const HeaderIconsContainer = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  return (
    <li data-testid="header-icons-container">
      <IconContainer>{children}</IconContainer>
    </li>
  );
};
