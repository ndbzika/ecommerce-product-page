import { Logo } from "./styled";

export const HeaderLogo = () => {
  return (
    <li>
      <a href="/">
        <Logo src="/images/logo.svg" alt="logo" data-testid="header-logo" />
      </a>
    </li>
  );
};
