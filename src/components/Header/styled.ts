import styled from "styled-components";

export const Header = styled.header`
  background: hsl(0, 0%, 100%);
  min-height: 12vh;
  max-height: 12vh;
  max-width: 80%;
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid hsl(0, 0%, 90%);
`;

export const List = styled.ul`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 5rem;

  & > li:nth-child(3) {
    margin-left: auto;
  }
`;

export const Logo = styled.img`
  min-width: 10rem;
  width: 100%;
`;

export const ListItems = styled.ul`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 2rem;
`;

export const Item = styled.li`
  font-size: 16px;
  transition: 0.2s;
  cursor: pointer;
  max-height: 30rem;
  height: 12vh;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 2px solid transparent;
  margin-bottom: -2px;

  & > a {
    color: hsl(219, 9%, 45%);
  }

  &:hover {
    border-bottom: 2px solid hsl(26, 100%, 55%);
  }
`;

export const IconContainer = styled.ul`
  min-width: 7rem;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

interface IconProps {
  size?: string;
}

export const Icon = styled.img<IconProps>`
  width: 25px;
  height: 25px;
  aspect-ratio: 4/3;
  ${({ size }) =>
    size &&
    `
    width: ${size};
    height: ${size};
  `}
`;
