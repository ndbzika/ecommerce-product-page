import { sneakers } from "../../utils/sneakers";
import {
  Container,
  Image,
  Slide,
  SliderContainer,
  ThumbImage,
  ThumbSlide,
} from "./styled";
import { Settings } from "react-slick";

import "./slick.css";
import "./slick-theme.css";

export const Carousel = () => {
  const settings: Settings = {
    customPaging: function (i) {
      return (
        <ThumbSlide>
          <ThumbImage src={`/images/image-product-${i + 1}-thumbnail.jpg`} />
        </ThumbSlide>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: "ondemand",
    arrows: false,
  };
  return (
    <Container>
      <SliderContainer {...settings}>
        {sneakers.map((sneaker) =>
          sneaker.images.normal.map((image, i) => (
            <Slide key={i}>
              <Image src={image} alt={sneaker.name} />
            </Slide>
          ))
        )}
      </SliderContainer>
    </Container>
  );
};
