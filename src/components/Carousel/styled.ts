import Slider from "react-slick";
import styled from "styled-components";

export const Container = styled.div`
  width: 30rem;
  height: 35rem;
  position: relative;
`;

export const SliderContainer = styled(Slider)`
  width: 100%;
  height: 32rem;

  .slick-thumb {
    bottom: -6rem;
    width: 100%;
    height: auto;
    display: flex !important;
    justify-content: space-between !important;
    align-items: center;
  }

  li[class="slick-active"] {
    border-radius: 0.8rem;
    border: 0.2rem solid hsl(26, 100%, 55%);
  }

  li[class="slick-active"] > a {
    opacity: 0.5;
  }

  li {
    width: 5.5rem;
    height: 5.5rem;
    margin: 0;
    padding: 0;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const Slide = styled.div``;

export const Image = styled.img`
  width: 100%;
  height: auto;
  border-radius: 0.8rem;
`;

export const ThumbSlide = styled.a`
  width: 100%;
  height: auto;
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border-radius: 0.8rem;
  transition: all 0.2s ease-in-out;

  &:hover {
    opacity: 0.5;
  }
`;

export const ThumbImage = styled.img`
  border-radius: 0.5rem;
`;
