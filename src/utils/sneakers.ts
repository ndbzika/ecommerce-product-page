export const sneakers = [
  {
    id: 1,
    name: "Fall Limited Edition Sneakers",
    description: `These low-profile sneakers are your perfect casual wear companion.
    Featuring a durable rubber outer sole, they’ll withstand everything the weather
    can offer.`,
    price: 250.0,
    discount: 50,
    company: "SNEAKER COMPANY",
    images: {
      normal: [
        "/images/image-product-1.jpg",
        "/images/image-product-2.jpg",
        "/images/image-product-3.jpg",
        "/images/image-product-4.jpg",
      ],
      thumbnail: [
        "/images/image-product-1-thumbnail.jpg",
        "/images/image-product-2-thumbnail.jpg",
        "/images/image-product-3-thumbnail.jpg",
        "/images/image-product-4-thumbnail.jpg",
      ],
    },
  },
];
