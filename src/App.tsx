import { Button } from "./components/Button";
import { Carousel } from "./components/Carousel";
import {
  InteractiveButtonsContainer,
  Main,
  ProductCompany,
  ProductContainer,
  ProductDescription,
  ProductDiscount,
  ProductDiscountContainer,
  ProductPrice,
  ProductPriceWithoutDiscount,
  ProductTitle,
  QuantityButton,
  QuantityContainer,
  QuantityInput,
} from "./styled";
import { sneakers } from "./utils/sneakers";

function App() {
  const handleApplyDiscount = (price: number, discount: number) => {
    return price - price * (discount / 100);
  };
  return (
    <Main>
      <div>
        <Carousel />
      </div>
      <div>
        {sneakers.map((sneaker) => (
          <ProductContainer key={sneaker.id}>
            <ProductCompany>{sneaker.company}</ProductCompany>
            <ProductTitle>{sneaker.name}</ProductTitle>
            <ProductDescription>{sneaker.description}</ProductDescription>
            <ProductDiscountContainer>
              <ProductPrice>
                ${handleApplyDiscount(sneaker.price, sneaker.discount)}.00
              </ProductPrice>
              <ProductDiscount>{sneaker.discount}%</ProductDiscount>
            </ProductDiscountContainer>
            <ProductPriceWithoutDiscount>
              ${sneaker.price}.00
            </ProductPriceWithoutDiscount>
          </ProductContainer>
        ))}
        <InteractiveButtonsContainer>
          <QuantityContainer>
            <QuantityButton borderside="left">
              <img src="/images/icon-minus.svg" alt="Minus" />
            </QuantityButton>
            <QuantityInput
              type="number"
              name="Count products"
              id="input-count-product"
              defaultValue={0}
            />
            <QuantityButton borderside="right">
              <img src="/images/icon-plus.svg" alt="Plus" />
            </QuantityButton>
          </QuantityContainer>
          <Button w="60%" h="3.2rem">
            <img src="/images/icon-cart-white.svg" alt="Shop Cart" />
            <span>Add to cart</span>
          </Button>
        </InteractiveButtonsContainer>
      </div>
    </Main>
  );
}

export default App;
