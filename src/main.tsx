import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import GlobalStyles from "./styles/GlobalStyles.ts";
import { Header } from "./components/Header/index.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <GlobalStyles />
    <Header.Root>
      <Header.Logo />
      <Header.ListItems>
        <Header.Item link="/collections">Collections</Header.Item>
        <Header.Item link="/men">Men</Header.Item>
        <Header.Item link="/women">Women</Header.Item>
        <Header.Item link="/about">About</Header.Item>
        <Header.Item link="/contact">Contact</Header.Item>
      </Header.ListItems>
      <Header.IconsContainer>
        <Header.IconButton icon="cart" />
        <Header.IconButton icon="profile" size="50px" />
      </Header.IconsContainer>
    </Header.Root>
    <App />
  </React.StrictMode>
);
