import styled from "styled-components";

export const Main = styled.main`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 7rem;
  height: 80vh;
`;

export const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  width: 32rem;
  height: 25rem;
`;

export const ProductCompany = styled.span`
  font-size: 0.9rem;
  font-weight: 700;
  letter-spacing: 0.1rem;
  color: hsl(26, 100%, 55%);
  text-transform: uppercase;
  margin: 1rem 0;
`;

export const ProductTitle = styled.h1`
  font-size: 3rem;
  font-weight: 700;
  line-height: 3rem;
  letter-spacing: 0.1rem;
  color: hsl(220, 13%, 13%);
  width: 100%;
  margin: -2rem 0 1rem 0;
`;

export const ProductDescription = styled.p`
  font-size: 1rem;
  line-height: 1.5rem;
  letter-spacing: 0.05rem;
  color: hsl(219, 9%, 45%);
`;

export const ProductDiscountContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ProductPrice = styled.span`
  font-size: 1.7rem;
  font-weight: 700;
  line-height: 1.5rem;
  letter-spacing: 0.1rem;
  color: hsl(220, 13%, 13%);
  margin-right: 0.5rem;
`;

export const ProductDiscount = styled.span`
  font-size: 1rem;
  font-weight: 700;
  line-height: 1.5rem;
  letter-spacing: 0.1rem;
  color: hsl(26, 100%, 55%);
  margin-right: 0.5rem;
  background-color: hsl(25, 100%, 94%);
  padding: 0.1rem 0.5rem;
  width: 3rem;
  border-radius: 0.6rem;
`;

export const ProductPriceWithoutDiscount = styled.span`
  font-size: 1rem;
  font-weight: 700;
  line-height: 1.5rem;
  letter-spacing: 0.05rem;
  color: hsl(220, 14%, 75%);
  text-decoration: line-through;
  margin: -1rem 0 2rem 0;
`;

export const InteractiveButtonsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 32rem;
  gap: 1rem;
  margin-top: 2rem;
`;

export const QuantityContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 10rem;
  height: 3rem;
`;

interface QuantityButtonProps {
  borderside: "left" | "right";
}

export const QuantityButton = styled.button<QuantityButtonProps>`
  ${({ borderside }) => {
    if (borderside === "left") {
      return "border-radius: 0.3rem 0 0 0.3rem;";
    }
    if (borderside === "right") {
      return "border-radius: 0 0.3rem 0.3rem 0;";
    }
  }}
  background-color: hsl(223, 64%, 98%);
  border: none;
  padding: 0.3rem 0.5rem;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30%;
  height: 100%;
`;

export const QuantityInput = styled.input`
  background-color: hsl(223, 64%, 98%);
  border: none;
  padding: 0.3rem 0.5rem;
  text-align: center;
  font-size: 1.2rem;
  font-weight: 700;
  width: 40%;
  height: 100%;
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  appearance: textfield;

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;
